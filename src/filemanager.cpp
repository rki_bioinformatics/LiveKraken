#include "filemanager.hpp"

namespace fs = boost::filesystem;

void get_tile_components(int tile, int &i1, int &i2, int &i3){
  i1 = tile / 1000;                 // Flowcell side
  i2 = (tile - 1000 * i1) / 100;    // Swath of the cell on this side
  i3 = tile - 1000 * i1 - 100 * i2; // One of the positions on this swath
}

// Given a tile, advance it to the next tile number.
// (I.e. 1216 -> 1301 or 1316 -> 2101)
int getNextTile(int tile, int max_tile = 0) {
  if (tile == max_tile) {
    return 0;
  }

  // Get the components of the tile number
  int i1, i2, i3;
  int m1, m2, m3;
  get_tile_components(tile, i1, i2, i3);
  get_tile_components(max_tile, m1, m2, m3);

  int overflow = 0;

  i3 += 1;
  if (i3 > m3) {
    i3 = 1;
    overflow = 1;
  }

  i2 += overflow;
  overflow = 0;
  if (i2 > m2) {
    i2 = 1;
    overflow = 1;
  }

  i1 += overflow;

  return i1 * 1000 + i2 * 100 + i3;
}


namespace kraken {

  BCLFileManager::BCLFileManager(std::string basecalls_folder, int length, int start_cycle, int step, int max_tile,
                                 std::vector<int> _target_tiles, std::vector<int> _target_lanes)
      :target_cycle(start_cycle), length(length), step(step), max_tile(max_tile),
       target_lanes(_target_lanes), target_tiles(_target_tiles){
    basecalls_path = fs::path(basecalls_folder);

    if (!fs::exists(basecalls_path)) {
      err(EX_NOINPUT, "%s not found.", basecalls_folder.c_str());
    }

    if (fs::is_regular_file(basecalls_path)) {
      valid = false;
      err(EX_NOINPUT, "File given. 'BaseCalls' directory expected.");
    }

    // Scan target directory and get all paths for each of the lane folders.
    this->getFilePaths();

  }

  bool BCLFileManager::is_valid() {
    return valid;
  }

  // Return the next tile we want to scan and return the cycle tiles
  // we have not read before. We return the bases from the last read tile
  // to the most recent available tile.
  TileInfo BCLFileManager::getTile() {
    if (!valid)
      return TileInfo();

    // when we have read all tiles
    if (active_tile == target_tiles.end()) {
      // advance to next lane and reset tile
      active_lane++;
      active_tile = target_tiles.begin();

      // went through all lanes, check if we are done
      // if not, reset to first lane.
      if (active_lane == target_lanes.end()) {
        if (end_reached) {
          valid = false;
          return TileInfo();
        }

        // reset lane and set next target cycle
        active_lane = target_lanes.begin();
        cycle_position = target_cycle;
        target_cycle = std::min(target_cycle + step, length - 1);
        end_reached = (target_cycle == length - 1);
      }
    }

    // wait for the target cycle for this tile and lane to appear
    while (!fs::exists(cyclePaths[*active_lane - 1][target_cycle])) {
      std::this_thread::sleep_for(std::chrono::seconds(5));
    }

    // return the [start,end] cycle indices as TileInfo, with tile and lane
    TileInfo ret = {*active_lane, *active_tile, cycle_position, target_cycle + end_reached};

    active_tile++;

    return ret;
  }

  void BCLFileManager::getFilePaths() {
    // copy those directories to the lanePaths vector that start with 'L'
    copy_if(directory_iterator(basecalls_path), directory_iterator(), back_inserter(lanePaths),
            [&](const fs::path &p) {
                return (p.filename().string()[0] == 'L');
            }
    );

    sort(lanePaths.begin(), lanePaths.end());

    // Get paths to all cycles in all lanes.
    for (fs::path &lane_path : lanePaths) {
      cyclePaths.push_back(std::vector<fs::path>());

      for (int i = 1; i <= length; ++i) {
        fs::path tmp(lane_path);
        tmp += ("/C" + std::to_string(i) + ".1");
        cyclePaths.back().push_back(tmp);
      }
    }

    // collect the lane numbers into a set
    // assumption: LXXX where XXX is the lane number
    std::unordered_map<int, int> lane_numbers;
    for (size_t i=0; i < lanePaths.size(); ++i) {
      int lane_num = std::stoi(lanePaths[i].filename().string().substr(1));
      lane_numbers[lane_num] = i+1;
    }

    std::vector<int> lane_selection;

    // check if target lanes exist
    for (int lane_num : target_lanes) {
      if (lane_numbers.count(lane_num) == 0) {
        copy(lanePaths.begin(), lanePaths.end(), std::ostream_iterator<fs::path>(std::cout, "\n"));
        errx(EX_NOINPUT, "Lane %i was not found. Please only select from existing lanes shown above", lane_num);
      }
      else{
        lane_selection.push_back(lane_numbers[lane_num]);
      }
    }

    // if no target lanes were specified, use all existing ones
    if (lane_selection.empty()) {
      for (size_t i = 1; i <= lanePaths.size(); ++i)
        lane_selection.push_back(i);
    }

    target_lanes = lane_selection;

    // if no target tiles given, iterate through all tiles
    if (target_tiles.empty()) {
      int tile = 1101;
      do {
        target_tiles.push_back(tile);
      } while ( (tile = getNextTile(tile, max_tile)) );
    }

    // Output the Lane numbers we are going to analyze.
    int lanes = target_lanes.size();
    std::cout << "Using " << lanes << (lanes == 1 ? " lane directory." : " lane directories.") << "\n";
    copy(target_lanes.begin(), target_lanes.end(), std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;

    // initialize file manager state by pointing at the first lanes/tiles to analyze
    active_lane = target_lanes.begin();
    active_tile = target_tiles.begin();
  }
}
